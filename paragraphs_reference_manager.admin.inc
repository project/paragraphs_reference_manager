<?php

/**
 * @file
 * Admin callback for Paragraph Reference Manager.
 */

/**
 * Build the settings and create form for managing references.
 */
function paragraphs_reference_manager_admin_form($form, &$form_state) {
  // Get list of all field instances grouped by entity type and bundle.
  $all_instances = field_info_instances();
  // Loop through the returned instances.
  foreach ($all_instances as $entity => $entity_group) {
    // Loop through each entity type grouping.
    foreach ($entity_group as $bundle => $bundle_group) {
      // Loop through each budnle type grouping.
      foreach ($bundle_group as $field_name => $field_info) {
        $type = $field_info['widget']['type'];
        $name = $field_name;
        $label = $field_info['label'];
        // Select only the fields that are paragraph reference.
        if ($type == 'paragraphs_embed') {
          // Create array with information needed to create form.
          $entity_ref_fields[$name]['label'] = $label;
          // Get allowed paragraphs for current bundle and reference field.
          $results = paragraphs_reference_manager_get_allowed_values($entity, $bundle, $field_name);
          foreach (unserialize($results['data']) as $result) {
            if (is_array($result)) {
              foreach ($result as $key => $values) {
                if ($key == 'allowed_bundles') {
                  $allowed_bundles[$bundle] = $values;
                  $allowed_bundles[$bundle]['entity'] = $entity;
                }
              }
            }
          }
          $entity_ref_fields[$name]['bundle'][$bundle] = $allowed_bundles;
        }
      }
    }
  }
  if (!isset($entity_ref_fields)) {
    // Set a default message if no paragraph reference fields are found.
    drupal_set_message('There are no paragraph reference fields on any of your bundles.', 'warning');
    return;
  }
  foreach ($entity_ref_fields as $field_name => $field_info) {
    $form_group_field_key = 'paragraph_reference_manager_' . $field_name;
    // Group options by paragraph reference field.
    $form[$form_group_field_key] = array(
      '#type' => 'fieldset',
      '#title' => $field_info['label'] . " ($field_name)",
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    foreach ($field_info['bundle'] as $bundle_name => $allowed_values) {
      // Set checkboxes to be unchecked by default.
      $default_val = 0;
      // Identify paragraph by argument from URL passed by hook_menu().
      $paragraph_bundle = $form_state['build_info']['args'][0];
      // If checkbox is checked on the bundle, mirror that behavior here.
      if ($allowed_values[$bundle_name][$paragraph_bundle] && $allowed_values[$bundle_name][$paragraph_bundle] != -1) {
        $default_val = 1;
      }
      // Get the entity name from the array we build with db info.
      $entity_name = $allowed_values[$bundle_name]['entity'];
      // Send entity, bundle & field in array key to be exploded in form_submit.
      $form_field_key = $entity_name . '__' . $bundle_name . '__' . $field_name;
      // Get info about entity in order to extract the label.
      if ($entity_name == 'paragraphs_item') {
        $entity_info = paragraphs_bundle_load($bundle_name);
        $bundle_label = $entity_info->name;
      }
      elseif ($entity_name == 'field_collection_item') {
        $bundle_label = $bundle_name;
      }
      else {
        $entity_info = entity_get_info($entity_name, $bundle_name);
        $bundle_label = $entity_info['bundles'][$bundle_name]['label'];
      }
      // Create checkbox for each bundle in group.
      $form[$form_group_field_key][$form_field_key] = array(
        '#type' => 'checkbox',
        '#title' => "$bundle_label ($bundle_name) - $entity_name",
        '#default_value' => $default_val,
      );
    }
  }
  $form['#submit'][] = 'paragraphs_reference_manager_admin_form_submit';
  return system_settings_form($form);
}

/**
 * Submit handler/function for admin form.
 */
function paragraphs_reference_manager_admin_form_submit($form, &$form_state) {
  // Gather all checked bundles per reference field from admin form.
  foreach ($form_state['input'] as $key => $checkbox) {
    if ($checkbox == 1) {
      // Separate field_name, bundle, and entity.
      $checked_box = explode("__", $key);
      // Get entity name.
      $entity = $checked_box[0];
      // Get bundle type.
      $bundle = $checked_box[1];
      // Get entity reference field.
      $field_name = $checked_box[2];
      // Get paragraph bundle passed from URL via hook_menu().
      $paragraph = $form_state['build_info']['args'][0];
      // Get current item 'Data' column values from field_config_instance table.
      $results = paragraphs_reference_manager_get_allowed_values($entity, $bundle, $field_name);
      // Convert serialized value to array that can be manipulated.
      $data = unserialize($results['data']);
      // Loop through allowed bundles from db.
      if ($data) {
        foreach ($data['settings']['allowed_bundles'] as $paragraph_bundle => $enabled) {
          // If the db says the bundle is disabled and
          // bundle from db matches current item in our enabled checkbox array.
          if ($enabled == -1 && $paragraph_bundle == $paragraph) {
            // Tell user of addition but don't mention already enabled bundles.
            drupal_set_message(
              t('Added %paragraph to %field_name on %bundle.',
                array(
                  '%paragraph' => $paragraph,
                  '%field_name' => $field_name,
                  '%bundle' => $bundle,
                )
              )
            );
          }
          // Get array of allowed bundle weights.
          $sorted_array = $data['settings']['bundle_weights'];
          // Sort the array alphabetically.
          ksort($sorted_array);
          // Loop through alphabetically sorted array of allowed bundles.
          foreach ($sorted_array as $paragraph_bundle => $weight) {
            // Make sure to only get the weight values of enabled bundles.
            if ($data['settings']['allowed_bundles'][$paragraph_bundle] != -1) {
              // If the paragraph is alphabetically before the allowed bundle.
              if (strcmp($paragraph, $paragraph_bundle) < 0) {
                // Set to a lighter weight so it appears before it in the list.
                $data['settings']['bundle_weights'][$paragraph] = $weight - 1;
                // Don't reset weight for every bundle that comes after.
                break;
              }
            }
          }
        }
      }
      // Set bundle to enabled in array from db.
      $data['settings']['allowed_bundles'][$paragraph] = $paragraph;
      // Convert back to serialized format in order to save correctly to db.
      $serialized_data = serialize($data);
      // Add back to the initial result return by querying the db.
      $results['data'] = $serialized_data;
      // Run an update query on the db to actually save the new values.
      paragraphs_reference_manager_set_allowed_values($entity, $bundle, $field_name, $results);
    }
    if ($checkbox == 0) {
      // Separate field_name, bundle, and entity.
      $unchecked_box = explode("__", $key);
      if (count($checked_box) == 3) {
        // Get entity name.
        $entity = $unchecked_box[0];
        // Get bundle type.
        $bundle = $unchecked_box[1];
        // Get entity reference field.
        $field_name = $unchecked_box[2];
        // Get paragraph bundle passed from URL via hook_menu().
        $paragraph = $form_state['build_info']['args'][0];
        // Get item 'Data' column values from field_config_instance table.
        $results = paragraphs_reference_manager_get_allowed_values($entity, $bundle, $field_name);
        // Convert serialized value to array that can be manipulated.
        $data = unserialize($results['data']);
        if ($data) {
          // Loop through allowed bundles from db.
          foreach ($data['settings']['allowed_bundles'] as $paragraph_bundle => $enabled) {
            // If the db says the bundle is disabled and bundle from
            // db matches current item in our enabled checkbox array.
            if ($enabled != -1 && $paragraph_bundle == $paragraph) {
              // Notify user of additions, not already enabled bundles.
              drupal_set_message(
                t('Removed %paragraph from %field_name on %bundle.',
                  array(
                    '%paragraph' => $paragraph,
                    '%field_name' => $field_name,
                    '%bundle' => $bundle,
                  )
                )
              );
            }
          }
        }
        // Set bundle to enabled in array from db.
        $data['settings']['allowed_bundles'][$paragraph] = -1;
        // Convert back to serialized format in order to save correctly to db.
        $serialized_data = serialize($data);
        // Add back to the initial result return by querying the db.
        $results['data'] = $serialized_data;
        // Run an update query on the db to actually save the new values.
        paragraphs_reference_manager_set_allowed_values($entity, $bundle, $field_name, $results);
      }
    }
  }
}

/**
 * Queries database to find which paragraph bundles are enabled for nodes.
 *
 * @param string $bundle
 *   A string indicating current bundle to be used in query.
 * @param string $field_name
 *   A string indicating the current entity reference field to be used in query.
 *
 * @return array
 *   An associative array with allowed paragraph bundles for the reference field
 *   by content type.
 */
function paragraphs_reference_manager_get_allowed_values($entity, $bundle, $field_name) {
  // Get all the fields for the current bundle.
  $results = db_select('field_config_instance', 'fci')
    ->fields('fci', array('data'))
    ->condition('entity_type', $entity, '=')
    ->condition('bundle', $bundle, '=')
    ->condition('field_name', $field_name, '=')
    ->execute()
    ->fetchAssoc();
  return $results;
}

/**
 * Updates database to set which paragraph bundles are enabled for nodes.
 *
 * @param string $bundle
 *   A string indicating current bundle to be used in query.
 * @param string $field_name
 *   A string indicating the current entity reference field to be used in query.
 * @param array $results
 *   An array with the values to be saved to db.
 */
function paragraphs_reference_manager_set_allowed_values($entity, $bundle, $field_name, array $results) {
  db_update('field_config_instance')
    ->fields(array('data' => $results))
    ->condition('entity_type', $entity, '=')
    ->condition('bundle', $bundle, '=')
    ->condition('field_name', $field_name, '=')
    ->execute();
}
